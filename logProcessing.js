var program = require('commander');
var lineReader = require('line-reader');
var fs = require('fs');

//Urls to be processed are given in the external json file(url.json).
var urls = JSON.parse(fs.readFileSync('url.json'));

/**
 * Preparing program to run via command line
 */
program.version('0.0.1')
  .option('-f, --file <path>')
  .parse(process.argv);

/**
 * Function to convert all given urls to regex urls for easy matching in future iterations.
 * Done as a separate function in order to avoid converting for each comparison.
 */
(function convertToRegExUrls() {
	for (var index in urls) {
		urls[index].regExUrl = new RegExp(urls[index].url.replace(/\{[\w]+\}/g, '[\\w]+').replace(/\//g, '\\\/') + ' ', 'g');
    }
})();

/**
 * Line Reader module used to read line by line a log file.
 **/
lineReader.eachLine(program.file, function(line) {
    matchUrl(line);
}).then(function () {
    for (var index in urls) {
		console.log('**********************************************************');
		console.log('URL: ' + urls[index].method + ' ' + urls[index].url);
		console.log('Total No. of times URL called: ' + (urls[index].noOfTimes ? urls[index].noOfTimes : 0));
		
		if (urls[index].noOfTimes) {
    		console.log('Average Response Time: ' + Math.round(urls[index].totalTime / urls[index].noOfTimes) + 'ms');
	    	console.log('Dyno that responded the most: ' + getMaxUsedDyno(urls[index].dyno));
		}
    }
});

/**
 * Function to return the maximum used dyno for a request
 *
 * @param dyno object of an url.
 * @return index of the max used dyno. 
 **/
function getMaxUsedDyno(dyno) {
	var max = -1;
	var maxIndex;
	
	for (var index in dyno) {
		if (dyno[index] > max) {
			maxIndex = index;
		}
	}

	return maxIndex;
}

/**
 * Function to match a line of log file to given urls and process the total time taken by the request,
 * total number of times the request is called, construct dyno object.
 *
 * @param one line of log file
 **/
function matchUrl(line) {
	for (var index in urls) {
		var methodIndex = line.indexOf('method=') + ('method=').length;
		var method = line.substr(methodIndex, line.indexOf(' ', methodIndex) - methodIndex);
		
	    if (line.match(urls[index].regExUrl) && urls[index].method === method) {
    	    var connectIndex = line.indexOf('connect=') + ('connect=').length;
	        var connect = parseInt(line.substr(connectIndex, line.indexOf(' ', connectIndex) - connectIndex), 10);
    	    var serviceIndex = line.indexOf('service=') + ('service=').length;
	        var service = parseInt(line.substr(serviceIndex, line.indexOf(' ', serviceIndex) - serviceIndex), 10);
			urls[index].totalTime = (urls[index].totalTime ? urls[index].totalTime : 0) + connect + service;
			urls[index].noOfTimes = (urls[index].noOfTimes ? urls[index].noOfTimes : 0) + 1;
            var dynoIndex = line.indexOf('dyno=') + ('dyno=').length;
            var dyno = line.substr(dynoIndex, line.indexOf(' ', dynoIndex) - dynoIndex);
			
			if (!urls[index].dyno) {
				urls[index].dyno = {};
			}
			
			urls[index].dyno[dyno] = (urls[index].dyno[dyno] ? urls[index].dyno[dyno] : 0) + 1;
		}
	}
}
